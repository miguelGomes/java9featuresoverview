package com.two;

import com.example.ClassOne;

public class ClassTwo {
    public static void main(String[] args) {
        //This module just have visibility on class one because it was exposed on classOne
        System.out.println(new ClassOne().getName());
    }
}
